const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const contentSchema = new Schema({
    content_main_img: { type: String, required: true },
    content_side_img: { type: String, required: true },
    content_main_label: { type: String, required: true },
    content_main_description: { type: String, required: true },
    content_main_description_de: { type: String, required: true },
    content_sub_img: { type: String, required: true },
    content_sub_label: { type: String, required: true },
    content_sub_description: { type: String, required: true },
    content_sub_img_2: { type: String, required: true },
    content_sub_label_2: { type: String, required: true },
    content_sub_description_2: { type: String, required: true },
    content_sub_img_3: { type: String, required: true },
    content_sub_label_3: { type: String, required: true },
    content_sub_description_3: { type: String, required: true },
    content_sub_img_4: { type: String, required: true },
    content_sub_label_4: { type: String, required: true },
    content_sub_description_4: { type: String, required: true },
    color: {type: String,required: true},
    date: { type: Date, required: true },

},    {
        timestamps:true,
});

const Content = mongoose.model('Content',contentSchema);

module.exports = Content;