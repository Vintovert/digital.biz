const router = require('express').Router();
let Content = require('../models/content.model');

router.route('/').get((req, res)=>{
    Content.find()
    .then(contents => res.json(contents))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res)=>{
    const content_main_img = req.body.content_main_img;
    const content_side_img = req.body.content_side_img;
    const content_main_label = req.body.content_main_label;
    const content_main_label_de = req.body.content_main_label_de;
    const content_main_description = req.body.content_main_description;
    const content_main_description_de = req.body.content_main_description_de;
    const content_sub_img = req.body.content_sub_img;
    const content_sub_label = req.body.content_sub_label;
    const content_sub_label_de = req.body.content_sub_label_de;
    const content_sub_description = req.body.content_sub_description_de;
    const content_sub_description_de = req.body.content_sub_description;
    const content_sub_img_2 = req.body.content_sub_img_2;
    const content_sub_label_2 = req.body.content_sub_label_2_de;
    const content_sub_label_2_de = req.body.content_sub_label_2;
    const content_sub_description_2 = req.body.content_sub_description_2;
    const content_sub_description_2_de = req.body.content_sub_description_2_de;
    const content_sub_img_3 = req.body.content_sub_img_3;
    const content_sub_label_3 = req.body.content_sub_label_3;
    const content_sub_label_3_de = req.body.content_sub_label_3_de;
    const content_sub_description_3 = req.body.content_sub_description_3;
    const content_sub_description_3_de = req.body.content_sub_description_3_de;
    const content_sub_img_4 = req.body.content_sub_img_4;
    const content_sub_label_4 = req.body.content_sub_label_4;
    const content_sub_label_4_de = req.body.content_sub_label_4_de;
    const content_sub_description_4 = req.body.content_sub_description_4;
    const content_sub_description_4_de = req.body.content_sub_description_4_de;
    const color = req.body.color;
    const date = Date.parse(req.body.date);

    const newContent = new Content({
        content_main_img,
        content_side_img,
        content_main_label,
        content_main_label_de,
        content_main_description,
        content_main_description_de,
        content_sub_img,
        content_sub_label,
        content_sub_label_de,
        content_sub_description,
        content_sub_description_de,
        content_sub_img_2,
        content_sub_label_2,
        content_sub_label_2_de,
        content_sub_description_2,
        content_sub_description_2_de,
        content_sub_img_3,
        content_sub_label_3,
        content_sub_label_3_de,
        content_sub_description_3,
        content_sub_description_3_de,
        content_sub_img_4,
        content_sub_label_4,
        content_sub_label_4_de,
        content_sub_description_4,
        content_sub_description_4_de,
        color,
        date
    });

    newContent.save()
    .then(()=>res.json('Content added!'))
    .catch(err => res.status(400).json('Error: ' + err));
});
router.route('/:id').get((req, res)=>{
    Content.findById(req.params.id)
    .then(content => res.json(content))
    .catch(err => res.status(400).json('Error: ' + err));
});
// router.route('/:id').delete((req, res)=>{
//     Content.findByIdAndDelete(req.params.id)
//     .then(content => res.json('Content deleted.'))
//     .catch(err => res.status(400).json('Error: ' + err));
// });
// router.route('/update/:id').post((req, res) => {
//     Exercise.findById(req.params.id) 
//     .then(exercise=>{
//         exercise.username = req.body.username;
//         exercise.description = req.body.description;
//         exercise.duration = Number(req.body.duration);
//         exercise.date = Date.parse(req.body.date);

//         exercise.save()
//         .then(()=> res.json('Exercise updated!'))
//         .catch(err => res.status(400).json('Error: ' + err));
//     })
//     .catch(err => res.status(400).json('Error: ' + err));
// });

module.exports = router;