import './App.css';
import './header-footer.css';
import './blog.css';
import './homepage.css';
import 'font-awesome/css/font-awesome.min.css';
import Main from './components/Main';
import DataProtection from './components/DataProtection';
import Blog from './components/Blog';
import Imprint from './components/Imprint';
import FAQ from './components/FAQ';
import ContentPage from './components/ContentPage';
import React, { Component, useState } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from './components/Header';
import ContactUs from './components/ContactUs';
import { Button, Modal } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { CookieBanner } from '@palmabit/react-cookie-law';

function App() {
  const { t } = useTranslation();
  const [showCookies, setCookieShow] = useState(false);

  const cookieClose = () => setCookieShow(false);
  const cookieShow = () => setCookieShow(true);

  const [showContact, setContactShow] = useState(false);

  const contactClose = () => setContactShow(false);
  const contactShow = () => setContactShow(true);

  const [showCookieSettings, setCookieSettingsShow] = useState(false);

  const cookieSettingsClose = () => setCookieSettingsShow(false);
  const cookieSettingsShow = () => setCookieSettingsShow(true);
  return (
    <div className="App">
      <Header />
      <Modal show={showContact} onHide={contactClose}>
        <ContactUs />
      </Modal>
      <Modal show={showCookieSettings} onHide={cookieSettingsClose}>
        {/* <div className="cookie_settings">
          <div className="w-100">
            <p className="description mt-5 ms-5 me-5" style={{ textAlign: 'left' }}>In order to provide various features on our website, better evaluate activities on our website, and always present to you suitable offers, we use cookies. Decide for yourself which cookies you would like to allow. By moving the respective cookie bar to blue and clicking on “Save settings“, you activate the corresponding cookie and agree that the cookie in question may be placed. You can reverse this on this page at any time.</p></div>
          <div className="scrollable d-flex">
            <div className="w-100">
              <p className="label_text">Required Cookies</p>
              <div className="d-flex switch_flex mt-3">
                <p className="switch_text mt-1">Language converstion</p>
                <label className="switch">
                  <input type="checkbox" />
                  <span className="slider round"></span>
                </label>
              </div>
              <p className="small_letters">To ensure that you can use our website in other languages as well.</p>
            </div>
            <div className="w-100">
              <p className="label_text">Functional Cookies</p>
              <div className="d-flex switch_flex mt-3">
                <p className="switch_text mt-1">YouTube</p>
                <label className="switch">
                  <input type="checkbox" />
                  <span className="slider round"></span>
                </label>
              </div>
              <p className="small_letters">To ensure that you can use our website in other languages as well.</p>
              <div className="d-flex switch_flex mt-3">
                <p className="switch_text mt-1">Landbot</p>
                <label className="switch">
                  <input type="checkbox" />
                  <span className="slider round"></span>
                </label>
              </div>
              <p className="small_letters">To improve the user experience, we make use of the tool Landbot.</p>
              <div className="d-flex switch_flex mt-3">
                <p className="switch_text mt-1">Google Maps</p>
                <label className="switch">
                  <input type="checkbox" />
                  <span className="slider round"></span>
                </label>
              </div>
              <p className="small_letters">In order for us to display the locations of universities, companies, and other stakeholders appearing our portal we use Google Maps.</p>
              <div className="d-flex switch_flex mt-3">
                <p className="switch_text mt-1">Google Analytics</p>
                <label className="switch">
                  <input type="checkbox" />
                  <span className="slider round"></span>
                </label>
              </div>
              <p className="small_letters">Anonymized data realitng to the use of the website is analyzed in order to further improve the offer on the website</p>
              <div className="d-flex switch_flex mt-3">
                <p className="switch_text mt-1">Unbound</p>
                <label className="switch">
                  <input type="checkbox" />
                  <span className="slider round"></span>
                </label>
              </div>
              <p className="small_letters">To display useful information for you prior to leaving the webiste</p>
            </div>
            <div className="w-100">
              <p className="label_text">Marketing Cookies</p>
              <div className="d-flex switch_flex mt-3">
                <p className="switch_text mt-1">Bing Ads</p>
                <label className="switch">
                  <input type="checkbox" />
                  <span className="slider round"></span>
                </label>
              </div>
              <div className="d-flex switch_flex mt-3">
                <p className="switch_text mt-1">Google Ads</p>
                <label className="switch">
                  <input type="checkbox" />
                  <span className="slider round"></span>
                </label>
              </div>
              <div className="d-flex switch_flex mt-3">
                <p className="switch_text mt-1">Facebook</p>
                <label className="switch">
                  <input type="checkbox" />
                  <span className="slider round"></span>
                </label>
              </div>
              <div className="d-flex switch_flex mt-3">
                <p className="switch_text mt-1">LinkedIn</p>
                <label className="switch">
                  <input type="checkbox" />
                  <span className="slider round"></span>
                </label>
              </div>
            </div>
          </div>
          <div className="cookie_settings_buttons d-flex mt-4">
            <button onClick={() => {cookieSettingsClose();cookieShow();}} style={{ width: '30%' }} className="cookie_button_2">BACK TO DESCRIPTION</button>
            <button style={{ backgroundColor: '#FFB300', width: '30%' }} className="cookie_button">SAVE CUSTOM SETTING</button>
            <button style={{ width: '30%' }} className="cookie_button ">ACCEPT ALL COOKIES</button>
          </div>
        </div> */}
      </Modal>
      <Modal show={showCookies} onHide={cookieClose}>
        <div className="cookieTest">
      <CookieBanner
      message="By clicking on the button, you agree to the use of cookies described here by the platform universitaet.com. At this point you can also object to the use of cookies or revoke your consent. Cookies are used to analyze your use of our websites and to personalize our services. Web cookies from third-party providers also give you personalized advertising, even if you no longer access our website."
      wholeDomain={true}
      onAccept = {() => {}}
      onAcceptPreferences = {() => {}}
      onAcceptStatistics = {() => {}}
      onAcceptMarketing = {() => {}}
    />
    </div>
        {/* <div className="cookies">
          <div className="w-100"><p className="mt-5 ms-5 me-5" style={{ textAlign: 'left' }}>By clicking on the button, you agree to the use of cookies described here by the platform universitaet.com. At this point you can also object to the use of cookies or revoke your consent. Cookies are used to analyze your use of our websites and to personalize our services. Web cookies from third-party providers also give you personalized advertising, even if you no longer access our website.</p></div>
          <div style={{ justifyContent: 'space-around' }} className="d-flex mt-4">
            <button className="cookie_button">ACCEPT AND CONTINUE</button>
            <button onClick={() => {cookieSettingsShow();cookieClose();}} className="cookie_button_2">COOKIE SETTINGS</button>
          </div>
        </div> */}
      </Modal>
      <Router>
        <Route path="/contents/:id" exact component={ContentPage} />
        <Route path="/" exact component={Main} />
        <Route path="/data-protection" exact component={DataProtection} />
        <Route path="/imprint" exact component={Imprint} />
        <Route path="/FAQ" exact component={FAQ} />
        <Route path="/blog" exact component={Blog} />
      </Router>
      <div className="Footer">
        <div className="d-flex footer_container">
          <img className="footer_image" src="../images/Final logo.png"></img>
          <div className="footer_links d-flex">
            <a className="anchor" href="/data-protection"><p>{t('Footer.1')}</p></a>
            <a className="anchor" href="/imprint"><p>{t('Footer.2')}</p></a>
            <a style={{ cursor: 'pointer' }} onClick={cookieShow} className="anchor" ><p>{t('Footer.3')}</p></a>
            <a className="anchor" href="/FAQ"><p>{t('Footer.4')}</p></a>
            <a className="anchor" href="/blog"><p>{t('Footer.5')}</p></a>
            <a style={{ cursor: 'pointer' }} onClick={contactShow} className="anchor"><p>{t('Footer.6')}</p></a>
          </div>
          <p className="copyright">© 2021 Copyright: Onsites Group</p>
        </div>
      </div>
    </div>
  );
}

export default App;
