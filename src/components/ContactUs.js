import React, { useRef,useState } from 'react';
import emailjs from 'emailjs-com';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import { faFileExcel } from '@fortawesome/free-solid-svg-icons';

const ContactUs = () => {
    const form = useRef();
    const { t } = useTranslation();
    const sendEmail = (e) => {
        e.preventDefault();

        emailjs.sendForm('service_nk2ga88', 'template_0zdqobi', form.current, 'user_6IsV5mzN4hHgibvKtFjiI')
            .then((result) => {
                console.log(result.text);
            }, (error) => {
                console.log(error.text);
            });
            e.target.reset()
    };
    return (
        <form className="contact_form" ref={form} onSubmit={sendEmail}>
            <div className="popup">
                <h4 className="mt-5">{t('ContactForm.1')}</h4>
                <h5 className="d-flex flex-start ms-5 mt-3">{t('ContactForm.2')}</h5>
                <input type="text" name="name"></input>
                <h5 className="d-flex flex-start ms-5 mt-4">{t('ContactForm.3')}</h5>
                <input type="email" name="email"></input>
                <h5 className="d-flex flex-start ms-5 mt-4">{t('ContactForm.4')}</h5>
                <textarea type="text" name="message"></textarea>
                <button className="intro_button mt-4 w-75" type="submit" value="send"><p className="request_now">{t('ContactForm.5')}</p></button>
            </div>
        </form>
    );
}
export default ContactUs;