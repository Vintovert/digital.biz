import React, { useState } from "react";
import 'reactjs-popup/dist/index.css';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

const Header = () => {

    const { t } = useTranslation();
    const  [showing, langShow] = useState(false);

  function langClick(lang) {
    langShow(!showing);
    i18next.changeLanguage(lang)
  }

    const [show, setShow] = useState(false);

    const handleClick = () => {
        setShow(!show);
    };
    

    return (
        <div className="header">
            <div className="d-flex flex-start navigationbar">
                <div className="navbar-blue w-25"><a href="/"><img className="final_logo" src="../images/Final logo.png"></img></a></div>
                <div className="navbar-lightblue w-75"></div>
            </div>
            <div className="d-flex flex-start navigationbar-clone">
                <div className="navbar-blue-clone w-25"></div>
                <div className="navbar-lightblue w-100 mt-4 d-flex">
                    <img onClick={()=>langClick('de')} className={showing ? "flag_hide" : "flag_show flag me-3"} src="..\images\germany-31017_640.png"></img>
                    <img onClick={()=>langClick('en')} className={showing ? "flag_show flag me-3" : "flag_hide"} src="..\images\england.png"></img>
                    <p className="me-5 mt-2 flag_name">{t('Language.1')}</p>
                </div>
                <i className="fa fa-bars fa-2x me-4 hamburger" onClick={handleClick}></i>
            </div>

            <div class="d-flex flex-end">
                <div className={show ? 'showing hamburger_items me-5' : 'not-showing hamburger_items me-5'}>
                    <li class="list_group_item">
                        <a href="/contents/61823e7e71a3b9052c1eb4fa">
                            <button class="menu_button">{t('HamburgerMenu.1')}</button>
                        </a>
                    </li>
                    <li class="list_group_item">
                        <a href="/contents/61811e70f627dd31732f4eb1">
                            <button class="menu_button">{t('HamburgerMenu.2')}</button>
                        </a>
                    </li>
                    <li class="list_group_item">
                        <a href="/contents/618247d471a3b9052c1eb52a">
                            <button class="menu_button">{t('HamburgerMenu.3')}</button>
                        </a>
                    </li>
                    <li class="list_group_item">
                        <a href="/contents/61824c5d71a3b9052c1eb55d">
                            <button class="menu_button">{t('HamburgerMenu.4')}</button>
                        </a>
                    </li>
                    <li class="list_group_item">
                        <a href="/contents/61824dd871a3b9052c1eb568">
                            <button class="menu_button">{t('HamburgerMenu.5')}</button>
                        </a>
                    </li>
                    <li class="list_group_item">
                        <a href="/contents/61824ed271a3b9052c1eb570">
                            <button class="menu_button">{t('HamburgerMenu.6')}</button>
                        </a>
                    </li>
                    <li class="list_group_item">
                        <a href="/contents/61824fe071a3b9052c1eb57b">
                            <button class="menu_button">{t('HamburgerMenu.7')}</button>
                        </a>
                    </li>
                </div>
            </div>
        </div>
        
    );
}

export default Header;
