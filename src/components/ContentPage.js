import React, { Component } from 'react';
import axios from 'axios';
import '../contentPage.css';
import { withTranslation } from 'react-i18next';
import { Carousel, Modal } from 'react-bootstrap';
import ContactUs from './ContactUs';
class ContentPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content_main_img: '',
      content_side_img: '',
      content_main_label: '',
      content_main_description: '',
      content_main_description_de: '',
      content_sub_img: '',
      content_sub_label: '',
      content_sub_description: '',
      content_sub_img_2: '',
      content_sub_label_2: '',
      content_sub_description_2: '',
      content_sub_img_3: '',
      content_sub_label_3: '',
      content_sub_description_3: '',
      content_sub_img_4: '',
      content_sub_label_4: '',
      content_sub_description_4: '',
      color: '',
      date: new Date(),
    }
  }
  componentDidMount() {
    axios.get('http://localhost:5000/contents/' + this.props.match.params.id)
      .then(response => {
        this.setState({
          content_main_img: response.data.content_main_img,
          content_side_img: response.data.content_side_img,
          content_main_label: response.data.content_main_label,
          content_main_label_de: response.data.content_main_label_de,
          content_main_description: response.data.content_main_description,
          content_main_description_de: response.data.content_main_description_de,
          content_sub_img: response.data.content_sub_img,
          content_sub_label: response.data.content_sub_label,
          content_sub_label_de: response.data.content_sub_label_de,
          content_sub_description: response.data.content_sub_description,
          content_sub_description_de: response.data.content_sub_description_de,
          content_sub_img_2: response.data.content_sub_img_2,
          content_sub_label_2: response.data.content_sub_label_2,
          content_sub_label_2_de: response.data.content_sub_label_2_de,
          content_sub_description_2: response.data.content_sub_description_2,
          content_sub_description_2_de: response.data.content_sub_description_2_de,
          content_sub_img_3: response.data.content_sub_img_3,
          content_sub_label_3: response.data.content_sub_label_3,
          content_sub_label_3_de: response.data.content_sub_label_3_de,
          content_sub_description_3: response.data.content_sub_description_3,
          content_sub_description_3_de: response.data.content_sub_description_3_de,
          content_sub_img_4: response.data.content_sub_img_4,
          content_sub_description_4: response.data.content_sub_description_4,
          content_sub_description_4_de: response.data.content_sub_description_4_de,
          content_sub_label_4: response.data.content_sub_label_4,
          content_sub_label_4_de: response.data.content_sub_label_4_de,
          color: response.data.color
        })
      })
      .catch(function (error) {
        console.log(error);
      })
  }
  openContactUs = () => this.setState({ isOpen: true });
  closeContactUs = () => this.setState({ isOpen: false });
  render() {
    const { t } = this.props;
    return (
      <div className="contentPage">
        <Modal show={this.state.isOpen} onHide={this.closeContactUs}>
          <ContactUs />
        </Modal>
        <div className="firstRow d-flex">
          <div className="side_text_container ">
            <div className="w-100">
              <img className="d-block contentPage_main_image" src={`../images/${this.state.content_main_img}`} alt="First slide" />
              <p className="contentPage_main_label" style={{ color:`${this.state.color}` }}>{t('ContentPage.1',{returnObjects: true, content_main_label: `${this.state.content_main_label}`, content_main_label_de: `${this.state.content_main_label_de}`})}</p>
              <p className="contentPage_main_description mt-4">{t('ContentPage.2',{returnObjects: true, content_main_description: `${this.state.content_main_description}`, content_main_description_de: `${this.state.content_main_description_de}`})}</p>
              <button onClick={this.openContactUs} className="contentPage_intro_button mt-4" style={{ background:`${this.state.color}` }}><p className="request_now">{t('ContentPage.request')}</p></button>
            </div>
          </div>
          <img className="d-block contentPage_side_image" src={`../images/${this.state.content_side_img}`} alt="First slide" />
        </div>
        <div className="subRow d-flex">
          <div className="side_text_container">
            <div className="w-85">
              <img className="d-block contentPage_main_image" src={`../images/${this.state.content_sub_img}`} alt="First slide" />
            </div>
          </div>
          <div className="contentPage_text_container w-100">
            <p className="contentPage_main_label" style={{ color:`${this.state.color}` }}>{t('ContentPage.3',{returnObjects: true, content_sub_label: `${this.state.content_sub_label}`, content_sub_label_de: `${this.state.content_sub_label_de}`})}</p>
            <p className="contentPage_main_description mt-4">{t('ContentPage.4',{returnObjects: true, content_sub_description: `${this.state.content_sub_description}`, content_sub_description_de: `${this.state.content_sub_description_de}`})}</p>
            <button onClick={this.openContactUs} className="contentPage_intro_button mt-4 w-25 me-5" style={{ background:`${this.state.color}` }}><p className="request_now">{t('ContentPage.request')}</p></button>
          </div>
        </div>
        <div className="subRow white d-flex">
          <div className="contentPage_text_container w-100">
            <p className="contentPage_main_label" style={{ color:`${this.state.color}` }}>{t('ContentPage.5',{returnObjects: true, content_sub_label_2: `${this.state.content_sub_label_2}`, content_sub_label_2_de: `${this.state.content_sub_label_2_de}`})}</p>
            <p className="contentPage_main_description mt-4">{t('ContentPage.6',{returnObjects: true, content_sub_description_2: `${this.state.content_sub_description_2}`, content_sub_description_2_de: `${this.state.content_sub_description_2_de}`})}</p>
            <button onClick={this.openContactUs} className="contentPage_intro_button mt-4 w-25 me-5" style={{ background:`${this.state.color}` }}><p className="request_now">{t('ContentPage.request')}</p></button>
          </div>
          <div className="side_text_container">
            <div className="w-85">
              <img className="d-block contentPage_main_image_white" src={`../images/${this.state.content_sub_img_2}`} alt="First slide" />
            </div>
          </div>

        </div>
        <div className="subRow d-flex">
          <div className="side_text_container">
            <div className="w-85">
              <img className="d-block contentPage_main_image" src={`../images/${this.state.content_sub_img_3}`} alt="First slide" />
            </div>
          </div>
          <div className="contentPage_text_container w-100">
            <p className="contentPage_main_label" style={{ color:`${this.state.color}` }}>{t('ContentPage.7',{returnObjects: true, content_sub_label_3: `${this.state.content_sub_label_3}`, content_sub_label_3_de: `${this.state.content_sub_label_3_de}`})}</p>
            <p className="contentPage_main_description mt-4">{t('ContentPage.8',{returnObjects: true, content_sub_description_3: `${this.state.content_sub_description_3}`, content_sub_description_3_de: `${this.state.content_sub_description_3_de}`})}</p>
            <button onClick={this.openContactUs} className="contentPage_intro_button mt-4 w-25 me-5" style={{ background:`${this.state.color}` }}><p className="request_now">{t('ContentPage.request')}</p></button>
          </div>
        </div>
        <div className="subRow white d-flex">
          <div className="contentPage_text_container w-67">
            <p className="contentPage_main_label" style={{ color:`${this.state.color}` }}>{t('ContentPage.9',{returnObjects: true, content_sub_label_4: `${this.state.content_sub_label_4}`, content_sub_label_4_de: `${this.state.content_sub_label_4_de}`})}</p>
            <p className="contentPage_main_description mt-4">{t('ContentPage.10',{returnObjects: true, content_sub_description_4: `${this.state.content_sub_description_4}`, content_sub_description_4_de: `${this.state.content_sub_description_4_de}`})}</p>
            <button onClick={this.openContactUs} className="contentPage_intro_button mt-4 w-25 me-5" style={{ background:`${this.state.color}` }}><p className="request_now">{t('ContentPage.request')}</p></button>
          </div>
          <div className="side_text_container">
            <div className="w-85">
              <img className="d-block contentPage_main_image_white" src={`../images/${this.state.content_sub_img_4}`} alt="First slide" />
            </div>
          </div>

        </div>
      </div>
    );
  }
}
export default withTranslation()(ContentPage);