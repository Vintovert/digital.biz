import React from "react";

const DataProtection = () => {

    return (
        <div className="data_protection">
            <h1 className="fw-bold mb-5" style={{ color: "#238097", marginTop: '100px', fontSize: "48px" }}>Data Protection</h1>
                <p className="" style={{marginLeft:'20vw',textAlign:'left',marginBottom:'100px',fontSize:'18px',width:'63%',color:'#878787'  }}>General information<br /> The present Privacy policy is intended to provide information to the user of the present website about the methods, scope and purposes of collection and usage of personal data by the website administrator ONSITES Group Ltd.<br/><br /> The website administrator treats the personal data of the users very seriously, in confidentiality and in accordance to the legislative provisions. Due to the usage of new technologies and the continuous improvement of the present website, the present Privacy policy may need to be modified, so we recommend all users to review the present Privacy policy regularly.<br/><br /> Please note that data transfer over the Internet is not absolutely secured. In order to increase the security level the website administrator has installed SSL certificate capable of identifying the users in the address field of the browser by the prefix „https://“ and the blocking symbol. When the SSL encoding system is enabled, by default, the data send to the website administrator cannot be read by third parties. However, you have to bear in mind that it is not possible to provide complete data protection against access by other parties.<br/><br /> The definitions of the terms used, such as "personal data” or "processing”, are provided in Article 4 of the General Data Protection Regulation.<br/><br/><br/><br /> Access data<br /> Based on legitimate interest, according to Article 6, paragraph 1, letter of the General Data Protection Regulation, the administrator of the present website collects the data about the website visits and keeps them as log files at the web site server. In such a way, the following data are recorded:<br/><br /> Site visited<br/><br /> Time of the visit<br/><br /> Quantity of sent data in bytes<br/><br /> Source or link through which the user has reached the site<br/><br /></p>
        </div>
    );
}

export default DataProtection;
