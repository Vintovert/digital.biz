import React, { Component, useRef } from 'react';
import { View, Text, Animated, StyleSheet, Easing } from 'react-native';
import { Carousel, Modal } from 'react-bootstrap';
import ContactUs from './ContactUs';
import { withTranslation } from 'react-i18next';
let i = 0;
let SEO = 0.54 + i;
let Product = 0.4 + i;
let Monitoring = 0.85 + i;
let Design = 0.99 + i;
let Content = 0.25 + i;
let WebD = 0.7 + i;
let Group = 0.10 + i;
let max = 0;

class Main extends Component {
  constructor() {
    super();
    this.animated = new Animated.Value(0);
    var inputRange = [0, 1];
    var outputRange = ['0deg', '360deg'];
    this.rotate = this.animated.interpolate({ inputRange, outputRange });
    outputRange = ['0deg', '-360deg'];
    this.rotateOpposit = this.animated.interpolate({ inputRange, outputRange });
  }
  state = {
    showContactUs: false,
    showSEO: false,
    showMonitoring: false,
    showDesign: false,
    showContent: false,
    showWebD: false,
    showGroup: false,
    showProduct: false,
    hideLabel: true,
  };
  openContactUs = () => this.setState({ isOpen: true });
  closeContactUs = () => this.setState({ isOpen: false });
  showSEO() {
    this.setState({
      showSEO: true,
      showMonitoring: false,
      showDesign: false,
      showContent: false,
      showWebD: false,
      showGroup: false,
      showProduct: false,
      hideLabel: false,
    });
  }
  showMonitoring() {
    this.setState({
      showSEO: false,
      showMonitoring: true,
      showDesign: false,
      showContent: false,
      showWebD: false,
      showGroup: false,
      showProduct: false,
      hideLabel: false,
    });
  }
  showDesign() {
    this.setState({
      showSEO: false,
      showMonitoring: false,
      showDesign: true,
      showContent: false,
      showWebD: false,
      showGroup: false,
      showProduct: false,
      hideLabel: false,
    });
  }
  showContent() {
    this.setState({
      showSEO: false,
      showMonitoring: false,
      showDesign: false,
      showContent: true,
      showWebD: false,
      showGroup: false,
      showProduct: false,
      hideLabel: false,
    });
  }
  showWebD() {
    this.setState({
      showSEO: false,
      showMonitoring: false,
      showDesign: false,
      showContent: false,
      showWebD: true,
      showGroup: false,
      showProduct: false,
      hideLabel: false,
    });
  }
  showGroup() {
    this.setState({
      showSEO: false,
      showMonitoring: false,
      showDesign: false,
      showContent: false,
      showWebD: false,
      showGroup: true,
      showProduct: false,
      hideLabel: false,
    });
  }
  showProduct() {
    this.setState({
      showSEO: false,
      showMonitoring: false,
      showDesign: false,
      showContent: false,
      showWebD: false,
      showGroup: false,
      showProduct: true,
      hideLabel: false,
    });
  }

  animate() {
    Animated.loop(
      Animated.timing(this.animated, {
        toValue: 1,
        duration: 10000,
        easing: Easing.linear,
      }),
    ).start();
  }
  animateChoiceSEO() {
    if (SEO >= max) {
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: SEO,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = SEO;
    }
    else {
      SEO = SEO + 1;
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: SEO,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = SEO;
      SEO = SEO - 1;
      i++;
      addition();
    }
  }
  animateChoiceMonitoring() {
    if (Monitoring >= max) {
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: Monitoring,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = Monitoring;
    }
    else {
      Monitoring = Monitoring + 1;
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: Monitoring,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = Monitoring;
      Monitoring = Monitoring - 1;
      i++;
      addition();
    }
  }
  animateChoiceDesign() {
    if (Design >= max) {
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: Design,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = Design;
    }
    else {
      Design = Design + 1;
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: Design,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = Design;
      Design = Design - 1;
      i++;
      addition();
    }
  }
  animateChoiceContent() {
    if (Content >= max) {
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: Content,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = Content;
    }
    else {
      Content = Content + 1;
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: Content,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = Content;
      Content = Content - 1;
      i++;
      addition();
    }
  }
  animateChoiceWebD() {
    if (WebD >= max) {
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: WebD,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = WebD;
    }
    else {
      WebD = WebD + 1;
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: WebD,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = WebD;
      WebD = WebD - 1;
      i++;
      addition();
    }
  }
  animateChoiceGroup() {
    if (Group >= max) {
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: Group,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = Group;
    }
    else {
      Group = Group + 1;
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: Group,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = Group;
      Group = Group - 1;
      i++;
      addition();
    }
  }
  animateChoiceProduct() {
    if (Product >= max) {
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: Product,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = Product;
    }
    else {
      Product = Product + 1;
      Animated.loop(
        Animated.timing(this.animated, {
          toValue: Product,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.linear,
        }),
      ).start();
      max = Product;
      Product = Product - 1;
      i++;
      addition();

    }
  }
  componentDidMount() {
    this.animate();
  }
  render() {
    const transform = [{ rotate: this.rotate }];
    const transform1 = [{ rotate: this.rotateOpposit }];
    const { t } = this.props;
    return (
      <div className="Homepage">
        <Modal show={this.state.isOpen} onHide={this.closeContactUs}>
          <ContactUs />
        </Modal>
        <div className="HomepageCarousel d-flex w-100">
          <div className="side_text_container col-md-6">
            <div className="side_text_container_2">
              <p className="intro">{t('Carousel.1')}</p>
              <p className="intro_long mt-4">{t('Carousel.2')}</p>
              <button onClick={this.openContactUs} className="intro_button mt-4"><p className="request_now">{t('Carousel.3')}</p></button>
            </div>
          </div>
          <Carousel className="Carousel col-md-6">
            <Carousel.Item>
              <img
                className="d-block carousel_image"
                src=".\images\slider-1.png"
                alt="First slide"
              />
              <Carousel.Caption>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block carousel_image"
                src=".\images\slider-1.png"
                alt="Second slide"
              />
              <Carousel.Caption>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block carousel_image"
                src=".\images\slider-1.png"
                alt="Third slide"
              />
              <Carousel.Caption>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
        </div>
        <div className="How_it_works">
          <div className="how_it_works_title_container">
            <p className="how_it_works_title">{t('TheBlueSection.1')}</p>
          </div>
          <div className="how_it_works_img_container">
            <img className="how_it_works_img" src="https://digital.biz/public/images/how_it_works.svg"></img>
          </div>
        </div>
        <div className="Quality_digital_services">
          <div className="Quality_digital_services_title_container">
            <p className="Quality_digital_services_title">{t('QualityDigitalServices.1')}</p>
          </div>
          <div className="Quality_digital_services_text_container mt-5">
            <p className="Quality_digital_services_text">{t('QualityDigitalServices.2')}</p>
          </div>
          <button onClick={this.openContactUs} className="Quality_digital_services_button mt-4"><p className="request_now">{t('QualityDigitalServices.3')}</p></button>
        </div>
        <div className="Define_your_digital_strategy">
          <div className="Define_your_digital_strategy_title_container">
            <p className="Define_your_digital_strategy_title">{t('DefineYourDigitalStrategy.1')}</p>
          </div>
          <div className="Define_your_digital_strategy_animation d-flex">
            <div className="spin">
              <View style={styles.container}>
                <Animated.View style={[styles.centerItem]}>
                  <img className="base" src=".\images\BASE.png"></img>
                </Animated.View>
                <Animated.View style={[styles.item, { transform }]}>
                  <Animated.View style={[styles.seo, { transform: transform1 }]}>
                    <img onClick={() => { this.animateChoiceSEO();this.showSEO()}} className="seo_img animation_img" src=".\images\SEO.png" alt="seo"></img>
                  </Animated.View>
                  <Animated.View style={[styles.monitoring, { transform: transform1 }]}>
                    <img onClick={() => { this.animateChoiceMonitoring();this.showMonitoring()}} className="design_img animation_img" src=".\images\Monitoring.png" alt="monitoring"></img>
                  </Animated.View>
                  <Animated.View style={[styles.design, { transform: transform1 }]}>
                    <img onClick={() => { this.animateChoiceDesign();this.showDesign()}} className="design_img animation_img" src=".\images\Design.png" alt="design"></img>
                  </Animated.View>
                  <Animated.View style={[styles.content, { transform: transform1 }]}>
                    <img onClick={() => { this.animateChoiceContent();this.showContent()}} className="content_img animation_img" src=".\images\Content.png" alt="content"></img>
                  </Animated.View>
                  <Animated.View style={[styles.webd, { transform: transform1 }]}>
                    <img onClick={() => { this.animateChoiceWebD();this.showWebD()}} className="web_img animation_img" src=".\images\WebD.png" alt="webd"></img>
                  </Animated.View>
                  <Animated.View style={[styles.group, { transform: transform1 }]}>
                    <img onClick={() => { this.animateChoiceGroup();this.showGroup() }} className="group_img animation_img" src=".\images\Group.png" alt="group"></img>
                  </Animated.View>
                  <Animated.View style={[styles.product, { transform: transform1 }]}>
                    <img onClick={() => { this.animateChoiceProduct(); this.showProduct() }} className="product_img animation_img" src=".\images\Product.png" alt="product"></img>
                  </Animated.View>
                </Animated.View>
              </View>
            </div>
            <div className="play_button">
              <i class="fas fa fa-3x fa-play"></i>
            </div>
            <div className="show mt-4">
              <div className={this.state.hideLabel ? "true first" : "hidden"}>
                <p className="first_text">{t('Animation.1')}</p>
              </div>
              <div className={this.state.showSEO ? "true second product" : "hidden"}>
                <img className="seo_img" src=".\images\SEO.png" alt="seo"></img>
                <p className="seo_text label">{t('Animation.8')}</p>
                <p className="seo_text sub">{t('Animation.8-1')}</p>
                <a href="/contents/618247d471a3b9052c1eb52a" type="button" className="mt-1" style={{backgroundImage: "linear-gradient(to right, #92278F, #AF62A7)"}}><p className="second_button_txt">{t('Animation.9')}</p></a>
              </div>
              <div className={this.state.showMonitoring ? "true second product" : "hidden"}>
                <img className="seo_img" src=".\images\Monitoring.png" alt="monitoring"></img>
                <p className="monitoring_text label">{t('Animation.6')}</p>
                <p className="monitoring_text sub">{t('Animation.6-1')}</p>
                <a href="/contents/61824fe071a3b9052c1eb57b" type="button" className="mt-1" style={{backgroundColor: "rgb(176, 48, 118)"}}><p className="second_button_txt">{t('Animation.9')}</p></a>
              </div>
              <div className={this.state.showDesign ? "true second product" : "hidden"}>
                <img className="design_img" src=".\images\Design.png" alt="design"></img>
                <p className="design_text label">{t('Animation.5')}</p>
                <p className="design_text sub">{t('Animation.5-1')}</p>
                <a href="/contents/61824ed271a3b9052c1eb570" type="button" className="mt-1" style={{backgroundColor: "rgb(73, 187, 127)"}}><p className="second_button_txt">{t('Animation.9')}</p></a>
              </div>
              <div className={this.state.showContent ? "true second product" : "hidden"}>
                <img className="content_img" src=".\images\Content.png" alt="content"></img>
                <p className="content_text label">{t('Animation.3')}</p>
                <p className="content_text sub">{t('Animation.3-1')}</p>
                <a href="/contents/61824c5d71a3b9052c1eb55d" type="button" className="mt-1" style={{backgroundColor: "rgb(55, 176, 226)"}}><p className="second_button_txt">{t('Animation.9')}</p></a>
              </div>
              <div className={this.state.showWebD ? "true second product" : "hidden"}>
                <img className="web_img" src=".\images\WebD.png"></img>
                <p className="webd_text label">{t('Animation.7')}</p>
                <p className="webd_text sub">{t('Animation.7-1')}</p>
                <a type="button" href="/contents/61823e7e71a3b9052c1eb4fa" className="mt-1" style={{backgroundColor: "rgb(239, 163, 57)"}}><p className="second_button_txt">{t('Animation.9')}</p></a>
              </div>
              <div className={this.state.showGroup ? "true second product" : "hidden"}>
                <img className="group_img" src=".\images\Group.png" alt="group"></img>
                <p className="group_text label">{t('Animation.4')}</p>
                <p className="group_text sub">{t('Animation.4-1')}</p>
                <a href="/contents/61824dd871a3b9052c1eb568" type="button" className="mt-1" style={{backgroundColor: "rgb(157, 67, 154)"}}><p className="second_button_txt">{t('Animation.9')}</p></a>
              </div>
              <div className={this.state.showProduct ? "true second product" : "hidden"} >
                <img className="product_img" src=".\images\Product.png" alt="product"></img>
                <p className="product_text label">{t('Animation.2')}</p>
                <p className="product_text sub">{t('Animation.2-1')}</p>
                <a href="/contents/61811e70f627dd31732f4eb1" type="button" className="mt-1" style={{backgroundColor: "rgb(240, 109, 65)"}}><p className="second_button_txt">{t('Animation.9')}</p></a>
              </div>

            </div>
          </div>
        </div>
        <div className="Digital_excellence">
          <div className="digital_excellence_title_container">
            <p className="digital_excellence_title">{t('DigitalExcellence.1')}</p>
          </div>
          <div className="digital_excellence_carousel mt-5">
            <Carousel>
              <Carousel.Item>
                <img
                  className="d-block digital_excellence_carousel_image"
                  src=".\images\pizza.png"
                  alt="Third slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block digital_excellence_carousel_image"
                  src=".\images\ghostwriting.png"
                  alt="Third slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block digital_excellence_carousel_image"
                  src=".\images\Studieren.png"
                  alt="Third slide"
                />
              </Carousel.Item>
            </Carousel>
          </div>
        </div>
        <div className="outstanding_keyword">
          <div className="outstanding_keyword_title_container">
            <p className="outstanding_keyword_title">{t('OutstandingKeyword.1')}</p>
          </div>
          <div className="outstanding_keyword_carousel mt-5">
            <Carousel>
              <Carousel.Item>
                <div className="outstanding_keyword_carousel_item">
                  <div className="outstanding_keyword_carousel_item_width">
                    <img
                      className="outstanding_keyword_carousel_image"
                      src=".\images\webdesign.biz.png"
                      alt="Third slide"
                    />
                    <p className="outstanding_keyword_carousel_label">malta.biz</p>
                  </div>
                  <div className="outstanding_keyword_carousel_item_width">
                    <img
                      className="outstanding_keyword_carousel_image"
                      src=".\images\webdesign.biz.png"
                      alt="Third slide"
                    />
                    <p className="outstanding_keyword_carousel_label">malta.biz</p>
                  </div>
                  <div className="outstanding_keyword_carousel_item_width">
                    <img
                      className="outstanding_keyword_carousel_image"
                      src=".\images\webdesign.biz.png"
                      alt="Third slide"
                    />
                    <p className="outstanding_keyword_carousel_label">malta.biz</p>
                  </div>
                </div>
              </Carousel.Item>
            </Carousel>
          </div>
          <button onClick={this.openContactUs} className="outstanding_keyword_button"><p className="request_now">{t('OutstandingKeyword.2')}</p></button>
        </div>
      </div>
    );
  }
}
export default withTranslation()(Main);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  item: {

    marginTop: '5.5vw',
    width: 100,
    height: '19vw', //this is the diameter here
  },
  content: {
    width: '100%',
    height: 20,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
  webd: {
    width: '100%',
    height: 20,
    position: 'absolute',
    marginTop: '17vw',
    marginLeft: '-3vw',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
  group: {
    width: '100%',
    height: 20,
    position: 'absolute',
    marginLeft: '7vw',
    marginTop: '3vw',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
  product: {
    width: '100%',
    height: 20,
    position: 'absolute',
    marginLeft: '-7vw',
    marginTop: '3vw',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
  design: {
    width: '100%',
    height: 20,
    position: 'absolute',
    marginLeft: '9vw',
    marginTop: '10vw',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
  monitoring: {
    width: '100%',
    height: 20,
    position: 'absolute',
    marginLeft: '5vw',
    marginTop: '16.5vw',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
  seo: {
    width: '100%',
    height: 20,
    position: 'absolute',
    marginLeft: '-9vw',
    marginTop: '11vw',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
  text: {
    color: '#fff',
  },
});

function addition() {
  SEO = 0.54 + i;
  Product = 0.4 + i;
  Monitoring = 0.85 + i;
  Design = 0.99 + i;
  Content = 0.25 + i;
  WebD = 0.7 + i;
  Group = 0.10 + i;
}
