import React from "react";

const Blog = () => {

    return (
        <div className="blog">
            <img className="blog_main_img" src="../images/blog.png"/>
            <h1 className="mt-5 mb-5" style={{ color: "#238097", marginTop: '100px', fontSize: "48px" }}>Blog</h1>
            <div className="d-flex blog_row">
                <div>
                    <img src="../images/zC2y8Nb9eA.png"/>
                    <p>Systematic order instead of chaos: New accents in IT investments in 2020</p>
                </div>
                <div>
                    <img src="../images/PwCrcglz6o.png"/>
                    <p>Programming language in comparison: Tops and flops of programming languages in the third decade of the 21st century</p>
                </div>
                <div>
                    <img src="../images/qHFUph5p1R.png"/>
                    <p>Key mobile technology application trends for 2020 every business should have</p>
                </div>
            </div>
            <div className="d-flex blog_row">
                <div>
                    <img src="../images/IHMbMl95VF.png"/>
                    <p>Digital sensation: New robots created from frog cells</p>
                </div>
                <div>
                    <img src="../images/Zne8q0oHMg.png"/>
                    <p>Deep learning as the digital trend of the 2020s</p>
                </div>
                <div>
                    <img src="../images/FxwHR5oVqE.png"/>
                    <p>Trends and challenges in the digital working world in the third decade of the 21st century</p>
                </div>
            </div>
        </div>
    );
}

export default Blog;
